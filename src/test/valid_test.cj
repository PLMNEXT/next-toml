package next_toml.test

import std.unittest.testmacro.Test
import std.unittest.testmacro.TestCase
import std.unittest.testmacro.Assert
import std.unittest.testmacro.ExpectThrows

@Test
class ValidTest {
    @TestCase
    func shouldFail1Test() {
        runValidTest("array-empty")
    }

    @TestCase
    func shouldFail2Test() {
        runValidTest("arrays-hetergeneous")
    }

    @TestCase
    func shouldFail3Test() {
        runValidTest("arrays-nested")
    }

    @TestCase
    func shouldFail4Test() {
        runValidTest("datetime")
    }

    @TestCase
    func shouldFail5Test() {
        runValidTest("empty")
    }

    @TestCase
    func shouldFail6Test() {
        runValidTest("example")
    }

    @TestCase
    func shouldFail7Test() {
        runValidTest("float")
    }

    @TestCase
    func shouldFail8Test() {
        runValidTest("implicit-and-explicit-before")
    }

    @TestCase
    func shouldFail9Test() {
        runValidTest("implicit-groups")
    }

    @TestCase
    func shouldFail10Test() {
        runValidTest("long-float")
    }

    @TestCase
    func shouldFail11Test() {
        runValidTest("long-integer")
    }

    @TestCase
    func shouldFail12Test() {
        runValidTest("key-special-chars-modified")
    }

    @TestCase
    func shouldFail13Test() {
        runValidTest("integer")
    }

    @TestCase
    func shouldFail14Test() {
        runValidTest("string-empty")
    }

    @TestCase
    func shouldFail15Test() {
        runValidTest("string-escapes-modified")
    }

    @TestCase
    func shouldFail16Test() {
        runValidTest("string-simple")
    }

    @TestCase
    func shouldFail17Test() {
        runValidTest("table-array-implicit")
    }

    @TestCase
    func shouldFail18Test() {
        runValidTest("table-array-many")
    }

    @TestCase
    func shouldFail19Test() {
        // Modified to remove stray spaces in the expected TOML
        // TODO
        // runEncoder("table-array-nest-modified",
        //     new TomlWriter.Builder().indentTablesBy(2).build());
    }

    @TestCase
    func shouldFail20Test() {
        runValidTest("table-array-one")
    }

    private func runValidTest(testName: String) {
        let filePath = Path("./test/valid/" + testName + ".toml")
        let data = File.readFrom(filePath)
        let contents = String.fromUtf8(data).replace("\r\n", "\n")

        let jsonPath = Path("./test/valid/" + testName + ".json")
        let jsonData = File.readFrom(jsonPath)
        let jsonDataStr = String.fromUtf8(jsonData)
        let jsonValue = JsonValue.fromStr(jsonDataStr)
        let rdata = enrichJson(jsonValue.asObject())
        let r = TomlWriter().write(rdata)
        @Assert(contents,r)
    }

    private func enrichJson(jsonObject: JsonObject): HashMap<String, Any> {
        let enriched = HashMap<String, Any>()
        let fileds = jsonObject.getFields()
        for (filed in fileds) {
            let value = enrichJsonValue(filed[1])
            enriched.add(filed[0], value)
        }
        return enriched
    }

    private func enrichJsonValue(jsonValue: JsonValue): Any {
        if (let Some(x) <- (jsonValue as JsonObject)) {
            if (x.containsKey("type") && x.containsKey("value")) {
                return enrichPrimitive(x)
            }
            return enrichJson(x)
        } else if (let Some(x) <- (jsonValue as JsonArray)) {
            let tables = ArrayList<Any>()
            for (item in x.getItems()) {
                tables.add(enrichJsonValue(item))
            }
            return tables
        }
        throw Exception("接收到了不支持的Json格式数据")
    }

    private func enrichPrimitive(jsonObject: JsonObject): Any {
        let t = jsonObject.get("type").getOrThrow()
        let value = jsonObject.get("value").getOrThrow()

        let jsonType = t.asString().getValue()
        if (jsonType == "bool") {
            return Bool.tryParse(value.toString())
        } else if (jsonType == "integer") {
            return Int64.tryParse(value.toString())
        } else if (jsonType == "float") {
            return Float64.tryParse(value.toString())
        } else if (jsonType == "string") {
            return value.asString().getValue()
        } else if (jsonType == "datetime") {
            let dateStr = value.asString().getValue()
            return DateTime.parse(dateStr)
        } else if (jsonType == "array") {
            let array = value.asArray()
            let enritchd = ArrayList<Any>()
            for (item in array.getItems()) {
                enritchd.add(enrichJsonValue(item))
            }
            return enritchd
        } else {
            throw Exception("不支持的数据类型")
        }
    }
}
