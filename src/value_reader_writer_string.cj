package next_toml

class ValueReaderWriterString <: ValueReader & ValueWriter {
    private static let UNICODE_REGEX = Regex("\\\\[uU](.{4})");

    static let INSTANCE = ValueReaderWriterString()
    private ValueReaderWriterString() {}

    public func canRead(r: Array<Rune>): Bool {
        return r[0] == r'"'
    }

    public func read(r: Array<Rune>, index: AtomicInt64, context: Context): Result<Any, Error> {
        index.fetchAdd(1)
        let startIndex = index.load()
        var endIndex = -1
        for (i in startIndex..r.size) {
            if (r[i] == r'"' && r[i - 1] != r'\\') {
                endIndex = i
                break
            }
            index.fetchAdd(1)
        }
        if (endIndex == -1) {
            context
                .error
                .unterminated(context.line, context.identifier.getName(), StringBuilder(r[startIndex - 1..]).toString())
        }
        if (context.error.hasError()) {
            return Err(context.error)
        }
        let raw = StringBuilder(r[startIndex..endIndex]).toString()
        // 
        var s = replaceUnicodeCharacters(raw)
        match (replaceSpecialCharacters(s)) {
            case Some(x) => s = x
            case None => context.error.invalidValue(context.line, context.identifier.getName(), raw)
        }
        if(context.error.hasError()) {
            return Err(context.error)
        }
        return Ok(JsonString(s))
    }

    public func canWrite(value: Any): Bool {
        if (value is String) {
            return true
        }
        return false
    }

    public func write(value: Any, context: WriteContext): Unit {
        context.write(r'"')
        if (let Some(s) <- (value as String)) {
            context.write(s)
        }
        context.write(r'"')
    }

    public func isPrimitiveType(): Bool {
        return true
    }

    /**
    替换unicode字符串
     */
    private func replaceUnicodeCharacters(value: String): String {
        let r = UNICODE_REGEX.matcher(value)
        let f = r.find()
        match (f) {
            case Some(x) => println(x.matchStr())
            case None => println("None")
        }
        return value
    }

    private func replaceSpecialCharacters(value: String): Option<String> {
        let r = value.toRuneArray()
        var isContinue = false

        for (i in 0..r.size - 1) {
            if (isContinue) {
                isContinue = false
                continue
            }
            if (r[i] == r'\\' && r[i + 1] == r'\\') {
                isContinue = true
            } else if (r[i] == r'\\' &&
                !(r[i + 1] == r'b' || r[i + 1] == r'f' || r[i + 1] == r'n' || r[i + 1] == r't' || r[i + 1] == r'r' ||
                r[i + 1] == r'"') || r[i + 1] == r'/') {
                return None
            }
        }
        return value
            .replace("\\n", "\n")
            .replace("\\\"", "\"")
            .replace("\\t", "\t")
            .replace("\\r", "\r")
            .replace("\\\\", "\\")
            .replace("\\/", "/")
            .replace("\\b", "\b")
            .replace("\\f", "\f")
    }
}
